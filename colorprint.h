#ifndef COLORPRINT_H
#define COLORPRINT_H

#define CPFUNC(name, type, ...) \
    name(&(type){__VA_ARGS__})

enum color {
    CP_COLOR_INVALID,
    CP_COLOR_BLACK,
    CP_COLOR_RED, 
    CP_COLOR_GREEN,
    CP_COLOR_YELLOW,
    CP_COLOR_BLUE,
    CP_COLOR_PURPLE,
    CP_COLOR_AQUA, 
    CP_COLOR_WHITE,
    CP_COLOR_END
};

enum fx {
    CP_FX_INVALID,
    CP_FX_PLAIN,
    CP_FX_BOLD,
    CP_FX_DARK,
    CP_FX_ITALIC,
    CP_FX_UNDERLINE,
    CP_FX_BLINK,
    CP_FX_BLINK2,
    CP_FX_REVERSE,
    CP_FX_HIDDEN,
    CP_FX_STRIKE,
    CP_FX_END
};

struct cpoptions {
    char *string;
    enum fx fx[CP_FX_END];
    enum color fg;
    enum color bg;
};

int cprint_default(struct cpoptions *cpo);
int cputs_default(struct cpoptions *cpo);

#define cprint(...) CPFUNC(cprint_default, struct cpoptions, __VA_ARGS__)
#define cputs(...) CPFUNC(cputs_default, struct cpoptions, __VA_ARGS__)

/*
 * Example: 
 * 
 *     cprint("Hello World", .fg = CP_COLOR_GREEN, .fx = {CP_FX_BOLD, CP_FX_ITALIC, CP_FX_STRIKE});
 * 
 * Or... 
 * 
 *     struct cpoptions cpo = {
 *         .string = "Hello World",
 *         .fg = CP_COLOR_GREEN,
 *         .fx = {
 *             CP_FX_BOLD,
 *             CP_FX_ITALIC,
 *             CP_FX_STRIKE
 *         }
 *     };
 * 
 *     cprint_default(&cpo);
 */

#endif
