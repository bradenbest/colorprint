#include "colorprint.h"

#include <stdio.h>

#define TN(test) fputs("test " test "\n\n        ", stderr);

// This is not a unit test.

void
test_cputs_default()
{ TN("cputs_default")

    struct cpoptions cpo = {
        .string = "IN SOVIET RUSSIA, BANANA EAT YOU",

        .fx = { CP_FX_BOLD },
        .fg = CP_COLOR_YELLOW,
        .bg = CP_COLOR_RED
    };

    cputs_default(&cpo);
    puts("");
}

void
test_cputs_string_only()
{ TN("cputs: string only")

    cputs("Hello World");
    puts("");
}

void
test_cputs_with_fg_green()
{ TN("cputs: fg = green")

    cputs("Hello, World!", .fg = CP_COLOR_GREEN);
    puts("");
}

void
test_cputs_with_fg_green_and_some_fx()
{ TN("cputs: fg (green), fx")

    cputs("Hello, World!", .fg = CP_COLOR_GREEN, .fx = {CP_FX_BOLD, CP_FX_ITALIC, CP_FX_STRIKE});
    puts("");
}

void
test_cputs_with_fg_green_bg_reg_and_some_fx()
{ TN("cputs: fg (green), bg(red), fx")

    cputs("Hello World", .fg = CP_COLOR_GREEN, .bg = CP_COLOR_RED, .fx = {CP_FX_BOLD, CP_FX_STRIKE, CP_FX_ITALIC});
    puts("");
}

void
test_cputs_with_fg_bg_and_lots_of_fx()
{ TN("cputs: fg, bg, fx (lots)")

    cputs( .string = "Hello World",
           .fg = CP_COLOR_BLUE,
           .bg = CP_COLOR_YELLOW,
           .fx = {
                CP_FX_DARK,
                CP_FX_BOLD,
                CP_FX_ITALIC,
                CP_FX_UNDERLINE,
                CP_FX_STRIKE
           });
    puts("");
}

void
test_cputs_with_too_many_fx()
{ TN("cputs: too many fx")

    cputs("Hello World", .fg = CP_COLOR_GREEN, .bg = CP_COLOR_RED, .fx = {
            CP_FX_BOLD,
            CP_FX_STRIKE,
            CP_FX_ITALIC,
            CP_FX_BOLD,
            CP_FX_BOLD,
            CP_FX_BOLD,
            CP_FX_BOLD,
            CP_FX_BOLD,
            CP_FX_BOLD,
            CP_FX_BOLD,
            CP_FX_BOLD,
            CP_FX_BOLD});
}

void
test_cputs_with_bad_fx()
{ TN("cputs: bad fx")

    cputs("Hello World", .fg = CP_COLOR_GREEN, .bg = CP_COLOR_RED, .fx = { CP_FX_END });
}

void
test_cputs_with_no_args()
{ TN("cputs: no args")

    cputs();
}

void
test_cputs_with_bad_fg()
{ TN("cputs: bad fg")

    cputs("Hello World", .fg = CP_COLOR_END);
}

void
test_cputs_with_bad_bg()
{ TN("cputs: bad bg")

    cputs("Hello World", .bg = CP_COLOR_END);
}

void
test_cputs_with_all_colors_fg()
{ TN("cputs: all colors (fg)");

    puts("");
    cputs("Black World",  .fg = CP_COLOR_BLACK);
    cputs("Red   World",  .fg = CP_COLOR_RED);
    cputs("Green World",  .fg = CP_COLOR_GREEN);
    cputs("Yellow World", .fg = CP_COLOR_YELLOW);
    cputs("Blue  World",  .fg = CP_COLOR_BLUE);
    cputs("Purple World", .fg = CP_COLOR_PURPLE);
    cputs("Aqua  World",  .fg = CP_COLOR_AQUA);
    cputs("White World",  .fg = CP_COLOR_WHITE);
    puts("");
}

void
test_cputs_with_all_colors_bg()
{ TN("cputs: all colors (bg)");

    puts("");
    cputs("Black World",  .bg = CP_COLOR_BLACK);
    cputs("Red   World",  .bg = CP_COLOR_RED);
    cputs("Green World",  .bg = CP_COLOR_GREEN);
    cputs("Yellow World", .bg = CP_COLOR_YELLOW);
    cputs("Blue  World",  .bg = CP_COLOR_BLUE);
    cputs("Purple World", .bg = CP_COLOR_PURPLE);
    cputs("Aqua  World",  .bg = CP_COLOR_AQUA);
    cputs("White World",  .bg = CP_COLOR_WHITE);
    puts("");
}

void
test_cputs_with_all_fx()
{ TN("cputs: all fx");

    puts("");
    cputs("Hello World",    .fx = {CP_FX_PLAIN});
    cputs("Hello Bold",     .fx = {CP_FX_BOLD});
    cputs("Hello Italic",   .fx = {CP_FX_ITALIC});
    cputs("Strike Through", .fx = {CP_FX_STRIKE});
    cputs("Under Line",     .fx = {CP_FX_UNDERLINE});
    cputs("Hello Dark",     .fx = {CP_FX_DARK});
    cputs("Hidden World",   .fx = {CP_FX_HIDDEN});
    cputs("Revered dlroW",  .fx = {CP_FX_REVERSE});
    cputs("Hello Blink",    .fx = {CP_FX_BLINK});
    cputs("Blink 2: Electric Boogaloo", .fx = {CP_FX_BLINK2});
    puts("");
}

int
main()
{
    test_cputs_default();
    test_cputs_string_only();
    test_cputs_with_fg_green();
    test_cputs_with_fg_green_and_some_fx();
    test_cputs_with_fg_green_bg_reg_and_some_fx();
    test_cputs_with_fg_bg_and_lots_of_fx();
    test_cputs_with_too_many_fx();
    test_cputs_with_bad_fx();
    test_cputs_with_no_args();
    test_cputs_with_bad_fg();
    test_cputs_with_bad_bg();
    test_cputs_with_all_colors_fg();
    test_cputs_with_all_colors_bg();
    test_cputs_with_all_fx();

    return 0;
}
