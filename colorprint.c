#include "colorprint.h"

#include <stdio.h>
#include <string.h>

#define CP_BUFSZ ((3 * 2) + (2 * (CP_FX_END - 1)) + 1)

enum validate_error {
    VE_OK,
    VE_FXTOOLARGE,
    VE_TOOMANYFX,
    VE_FGTOOLARGE,
    VE_BGTOOLARGE,
    VE_BADSTRING,
    VE_END
};

const char *validate_error_str[VE_END] = {
    [VE_FXTOOLARGE] = "(.fx) one or more effect values are out of bounds",
    [VE_TOOMANYFX]  = "(.fx) too many effects (the \"trap\" effect was set)",
    [VE_FGTOOLARGE] = "(.fg) foreground color out of bounds",
    [VE_BGTOOLARGE] = "(.bg) background color out of bounds",
    [VE_BADSTRING]  = "(.string) no string provided"
};

static void
apply_effect(char *out, enum fx fx)
{
    char buf[3] = { '0' + (fx - 1), ';' };

    strcat(out, buf);
}

static void
apply_fg(char *out, enum color c)
{
    char buf[4] = { '3', '0' + (c - 1), 'm' };

    strcat(out, buf);
}

static void
apply_bg(char *out, enum color c)
{
    char buf[4] = { '4', '0' + (c - 1), ';' };

    strcat(out, buf);
}

static void
early_terminate(char *out)
{
    out[strlen(out) - 1] = 'm';
}

static void
apply_fx(char *out, enum fx *fx)
{
    for(int i = 0; i < CP_FX_END - 1 && fx[i]; i++)
        apply_effect(out, fx[i]);
}

static void
apply_colors(char *out, struct cpoptions *cpo)
{
    if(cpo->bg)
        apply_bg(out, cpo->bg);

    if(cpo->fg)
        apply_fg(out, cpo->fg);
    else
        early_terminate(out);
}

static enum validate_error
validate_cpo(struct cpoptions *cpo)
{
    for(int i = 0; i < CP_FX_END - 1 && cpo->fx[i]; i++)
        if(cpo->fx[i] >= CP_FX_END)
            return VE_FXTOOLARGE;

    if(cpo->fx[CP_FX_END - 1] != CP_FX_INVALID)
        return VE_TOOMANYFX;

    if(cpo->fg >= CP_COLOR_END)
        return VE_FGTOOLARGE;

    if(cpo->bg >= CP_COLOR_END)
        return VE_BGTOOLARGE;

    if(cpo->string == NULL)
        return VE_BADSTRING;

    return VE_OK;
}

int
cprint_default(struct cpoptions *cpo)
{
    char options[CP_BUFSZ] = "";
    enum validate_error ve;

    if((ve = validate_cpo(cpo)) != VE_OK){
        fprintf(stderr, "colorprint: An error occurred. Details: E%03u (%s)\n", ve, validate_error_str[ve]);
        return -1;
    }

    apply_fx(options, cpo->fx);
    apply_colors(options, cpo);

    if(!cpo->fg && !cpo->bg && !cpo->fx[0])
        return fputs(cpo->string, stdout);
    else
        return printf("\033[%s%s\033[0m", options, cpo->string);
}

int
cputs_default(struct cpoptions *cpo)
{
    int ret = cprint_default(cpo);

    putchar('\n');
    return ret;
}
