# colorprint

colorprint is a simple, lightweight library/module for printing colorful output to your terminal.

It's useful when you just want to print colorful output, and libraries like ncurses and termcap are a little...*much*.

colorprint is exclusively for ANSI-compliant terminals (I.e. `UNIX`). It won't work on Windows unless you're using a terminal that supports ANSI espace sequences. Like PuTTY

# Installation

There is no installation process. You just throw `colorprint.c` and `colorprint.h` into your source directory and use them

# Usage

To use colorprint, just include the header and use one of its two functions.

    int cprint(...);
        prints `string`

    int cputs(...);
        prints `string`, but with a trailing newline

The arguments for `cprint` and `cputs` are the same. They each take a `struct cpoptions`:

    struct cpoptions {
        char *string;
        enum fx fx[10];
        enum color fg;
        enum color bg;
    };

`cprint` and `cputs` are macros that wrap `cprint_default` and `cputs_default` to enable a python-like calling interface.

The best way to understand how to use them is to read `colorprint.h`.

# Examples

print "Hello World\n" in plain text:

    cputs("Hello World")

print "Hello World\n" in bold with a blue foreground color:

    cputs("Hello World", .fg = CP_COLOR_BLUE, .fx = {CP_FX_BOLD});

print "Hello World\n" in an ugly style with tons of effects:

    cputs( .string = "Hello World",
           .fg = CP_COLOR_BLUE,
           .bg = CP_COLOR_YELLOW,
           .fx = {
                CP_FX_DARK,
                CP_FX_BOLD,
                CP_FX_ITALIC,
                CP_FX_UNDERLINE,
                CP_FX_STRIKE
           });

# License

colorprint is Public Domain.
